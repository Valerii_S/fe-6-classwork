import {GET_ALIAS, SET_NAME} from '../action/User'

const initialState = {
    userName: 'Psina',
    alias: 'guest'
};

export function userReducer(state = initialState, action) {
    switch (action.type) {
        case GET_ALIAS:
            return {...state, alias: action.payload};
        case SET_NAME:
            return {...state, userName: action.payload};
        default:
            return state
    }
}
