import {SET_AGE} from '../action/Age'

const initialState = {
    userAge: '6.2',
};

export function ageReducer(state = initialState, action) {
    switch (action.type) {
        case SET_AGE:
            return {...state, userAge: action.payload};
        default:
            return state
    }
}