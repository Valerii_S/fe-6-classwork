export const GET_ALIAS = 'GET_ALIAS';
export const SET_NAME = 'SET_NAME';

export function showAlias(alias) {
    return {
        type: GET_ALIAS,
        payload: alias
    }
}

export const setName = (name) => {
    return {
        type: SET_NAME,
        payload: name
    }
}

