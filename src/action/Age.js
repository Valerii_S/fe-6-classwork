export const SET_AGE = 'SET_AGE';

export const setAge = (age) => {
   return {
       type: SET_AGE,
       payload: age
   }
};
