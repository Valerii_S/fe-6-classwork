import React from 'react';
import {connect} from 'react-redux';

import Navigation from "./MyButton/Navigation";
import Navbar from "./Components/Navbar/Navbar";
import {showAlias, setName} from "./action/User";
import {setAge} from "./action/Age";


class App extends React.Component {
    state = {
        navbarClicked: false,
        color: this.props.color,
    };

    render() {
        const {name, age, alias} = this.props;
        const {setUserAge} = this.props;

        return (
            <div>
                <Navigation/>
                <h2>Name: {name.userName}<br/>Age: {age.userAge}</h2>
                <h2>{name.alias}</h2>
                <Navbar
                    color={this.state.color}
                    onClick={this.showSubmit.bind(this)}
                />

                {this.click ? <h1>Hello world</h1> : <h1>Or not Hello!</h1>}
                <button onClick={this.onBtnClick.bind(this)}>get alias</button>
                <button onClick={() => this.setNameBtn()}>set name</button>
                <button onClick={this.setNameBtn2}>set name2</button>
                <button onClick={() => setUserAge(88)}>set age</button>
            </div>)
    }

    showSubmit(click) {
        console.log('---click from APP.js', click.toString());
        this.click = click;
        this.setState(this.state, (click) => this.state.navbarClicked = click);
        console.log(this.props);

    };

    onBtnClick() {
        this.props.getAlias('gus');
    }

    setNameBtn() {
        this.props.setUserName('vasya');
    }

    setNameBtn2 = () => {
        this.props.setUserName('kolya');
    }

}

const mapDispatchToProps = dispatch => {
    return {
        getAlias: alias => dispatch(showAlias(alias)),
        setUserName: name => dispatch(setName(name)),
        setUserAge: age => dispatch(setAge(age))
    }
};


const mapStateToProps = store => {
    // console.log(store);
    return {
        name: store.userName,
        age: store.userAge,
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(App);
