import {BrowserRouter, Link, Route} from "react-router-dom";
import React from 'react';


const Home = () => (
    <div>
        <h2>Home</h2>
    </div>
);
const About = () => (
    <div>
        <h2>About</h2>
    </div>
);
const Contacts = () => (
    <div>
        <h2>Contacts</h2>
    </div>
);
class Navigation extends React.Component{
    render (){
        return (
            <BrowserRouter>
                <div>
                    <ul>
                        <li><Link to ="/home">Home</Link></li>
                        <li><Link to ="/about">About</Link></li>
                        <li><Link to ="/contacts">Contacts</Link></li>
                    </ul>
                    <hr/>
                    <Route path ="/home" component={Home}/>
                    <Route path ="/about" component={About}/>
                    <Route path ="/contacts" component={Contacts}/>
                </div>
            </BrowserRouter>
        )
    }
}
 export default Navigation;